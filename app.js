import Resources from "./src/Resources.js";
import GameTemplate from "./src/game/GameTemplate.js";
import Motor3DEngine from "./src/Motor3DEngine.js";

let engine = null;
let fpsCounter = null;

window.onload = (event) => {

    engine = new Motor3DEngine({
        resourcesURL: "resources.json",
        callback: onMotor3DEngine,
        width: 360,
        height: 270,
        hasPhysics: true,
        debug: true
    });

    fpsCounter = document.getElementById("fps-label");

    document.getElementById("play-pause-button").onclick = (event) => {
        onPlayPause(event.target);
    }

    document.getElementById("restart-button").onclick = (event) => {
        location.reload();
    }

    document.getElementById("physics-button").onclick = (event) => {
        onPhysics(event.target);
    }
}

const onPlayPause = (elem) => {

    let color = "";
    if(engine.isRunning) {
        color = "var(--grey-1)";
        engine.pause();
        elem.innerHTML = "RUN";
    } else {
        color = "var(--red)";
        engine.run();
        elem.innerHTML = "PAUSE";
    }
    elem.style.backgroundColor = color;
}

const onPhysics = (elem) => {

    let color = "";
    if(engine.isPhysics) {
        color = "var(--grey-1)";
        engine.pausePhysics();
        elem.innerHTML = "PHYSICS: OFF";
    } else {
        color = "var(--green)";
        engine.runPhysics();
        elem.innerHTML = "PHYSICS: ON";
    }

    elem.style.backgroundColor = color;
}

const onMotor3DEngine = (event) => {

    switch(event.action) {

        case Motor3DEngine.Actions.LOADING_DATA:
        break;

        case Motor3DEngine.Actions.DATA_LOADED:

        const gizmoGame = new GameTemplate();
        gizmoGame.setData("game.json").
        then(game => {
            engine.setGame(game);
            document.getElementById("play-pause-button").click();
        });
        break;

        case Motor3DEngine.Actions.FRAME:
        fpsCounter.innerHTML = event.data;
        break;

        case Motor3DEngine.Actions.RUNNING:
        break;

        case Motor3DEngine.Actions.PAUSED:
        break;
    }
}