import Display from "./core/Display.js";
import Resources from "./Resources.js";
import Time from "./utils/Time.js";
import Input from "./input/Input.js";
import Keyboard from "./input/Keyboard.js";

class Motor3DEngine {

    constructor(data) {

        this.callback = data.callback;
        if (!this.callback) throw new Error("No callback defined!");

        this.debug = data.debug;
        this.display = null;
        this.physics = null;
        this.game = null;
        this.isRunning = false;
        this.isPhysics = data.hasPhysics;

        if (data.fps) {
            Motor3DEngine.FPS = data.fps;
        }
        if (this.isPhysics) {
            this.physics = new Worker("./src/physics/physicsThread.js", { type: "module" });
            this.physics.addEventListener("message", (event) => {

                this.game.updatePhysics(event.data);
                // console.log(e.data);
            }, false);
        }

        this.callback({ action: Motor3DEngine.Actions.LOADING_DATA });
        Resources.loadData(data.resourcesURL).then(result => {
            this.init(data);
        });
    }

    init(data) {

        Input.create();
        this.display = new Display({
            view: document.getElementById("canvas-container"),
            width: data.width,
            height: data.height
        });
        this.target = this.display.frameBuffer;
        this.callback({ action: Motor3DEngine.Actions.DATA_LOADED });
    }

    setGame(game) {

        this.game = game;
        this.game.setRenderer(this.target);
        if (this.isPhysics) {
            this.physics.postMessage({ action: "addobjects", objects: this.game.getGameObjects() })
        }
    }

    run() {

        this.isRunning = true;

        if (this.physics) {
            this.runPhysics();
        }

        let frames = 0;
        let frameCounter = 0;
        const frameRate = 1.0 / Motor3DEngine.FPS;

        let instance = this;

        let lastTime = Time.getTime();
        let unprocessedTime = 0;

        let loop = (e) => {

            let render = false;

            let startTime = Time.getTime();
            let passedTime = startTime - lastTime;
            lastTime = startTime;

            unprocessedTime += (passedTime / Time.SECOND);
            frameCounter += passedTime;

            while (unprocessedTime > frameRate) {

                render = true;
                unprocessedTime -= frameRate;

                if (!this.isRunning) {
                    window.clearInterval(interID);
                }

                Time.setDelta(frameRate);

                Input.update();

                instance.game.update(Time.getDelta());

                this.display.swapBuffers();

                if (Input.getKey(Keyboard.ESCAPE)) {
                    instance.pause();
                }

                if (frameCounter >= Time.SECOND) {
                    if (this.debug) {
                        this.callback({
                            action: Motor3DEngine.Actions.FRAME,
                            data: frames
                        });
                    }
                    frames = 0;
                    frameCounter = 0;
                }
            }

            if (render) {

                if (this.isPhysics && this.isPhysics) {
                    this.physics.postMessage({
                        action: "getsimulation",
                        data: {
                            up: Input.isUp(),
                            down: Input.isDown(),
                            left: Input.isLeft(),
                            right: Input.isRight()
                        }
                    });
                }

                instance.game.render(Time.getDelta());
                frames++;
            }
        }

        let interID = window.setInterval(loop, 1);

        this.callback({ action: Motor3DEngine.Actions.RUNNING });
        // this.cleanUp();
    }

    pause() {
        this.isRunning = false;
        if (this.physics) {
            this.pausePhysics();
        }
        this.callback({ action: Motor3DEngine.Actions.PAUSED });
    }

    runPhysics() {
        this.isPhysics = true;

        if (this.isPhysics) {
            this.physics.postMessage({ action: "run" });
        }
    }

    pausePhysics() {
        this.isPhysics = false;
        this.physics.postMessage({ action: "pause" });
    }
}

Motor3DEngine.Actions = {
    LOADING_DATA: "motor3dengineloadingdata",
    DATA_LOADED: "motor3denginedataloaded",
    RUNNING: "motor3denginerunning",
    PAUSED: "motor3denginepaused",
    FRAME: "motor3dengineframe"
}

Motor3DEngine.FPS = 60;

export default Motor3DEngine