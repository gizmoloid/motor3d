/**
 * Implementation of a Axis Align Bounding Box volume collider
 * It follows the center-radius representation.
 */
class AABB extends Collider {

    /**
     * @param {PVec4} center 
     * @param {PVec4} radius 
     */
    constructor(center = new PVec4(), radius = new PVec4(0.5, 0.5, 0.5)) {
        super(Collider.Type.AABB);

        this.center = center;
        this.radius = radius;
    }

    /**
     * @param {AABB} aabbA 
     * @param {PMat4} mat 
     * @param {PVec4} translation 
     * @param {AABB} aabbB 
     */
    updateAABB(aabbA, mat, translation, aabbB) {

        aabbB.getCenter().x = translation.x;
        aabbB.getCenter().y = translation.y;
        aabbB.getCenter().z = translation.z;

        aabbB.getRadius().x = 0;
        aabbB.getRadius().y = 0;
        aabbB.getRadius().z = 0;

        const m = mat.getM();
        for (let i = 0; i < m.length; i++) {
            const element = m[i];

            aabbB.getCenter().x += m[i][0] * aabbA.getCenter().x;
            aabbB.getCenter().y += m[i][1] * aabbA.getCenter().y;
            aabbB.getCenter().z += m[i][2] * aabbA.getCenter().z;

            aabbB.getRadius().x += Math.abs(m[i][0]) * aabbA.getRadius().x;
        }
    }

    testAABB(other) {



        




        if(Math.abs(this.center.x - other.center.x) > (this.radius.x + other.radius.x)) {
            return new CollisionData(false, null);
        }
        if(Math.abs(this.center.y - other.center.y) > (this.radius.y + other.radius.y)) {
            return new CollisionData(false, null);
        }
        if(Math.abs(this.center.z - other.center.z) > (this.radius.z + other.radius.z)) {
            return new CollisionData(false, null);
        }

        const distancesA = other.center.subtractV(this.center);
        const distancesB = this.center.subtractV(other.center);

        return new CollisionData(true, distancesA.maxV(distancesB));
    }
    /**
     * OVERRIDES
     */

    /**
     * @param {PVec4} translation 
     */
    transform(translation) {
        this.center.plusEqualsV(translation);
    }
    /**
     * @returns {PVec4} position of the sphere in 3d
     */
    getCenter() {
        return this.center;
    }
}