class BoundingSphere extends Collider{

    constructor(center = new PVec4(), radius = 1) {
        super(Collider.Type.SPHERE);
        this.center = center;
        this.radius = radius;
    }

    /**
     * 
     * @param {BoundingSphere} other 
     */
    intersectBoundingSphere(other) {

        const radiusDistance = this.radius + other.getRadius();
        let direction = other.getCenter().subtractV(this.center);
        const centerDistance = direction.length();

        direction.divideEqualsN(centerDistance);

        const distance = centerDistance - radiusDistance;

        return new IntersectData(distance < 0, direction.multiplyN(distance));
    }

    /**
     * OVERRIDES
     */

    /**
     * @param {PVec4} translation 
     */
    transform(translation) {
        this.center.plusEqualsV(translation);
    }

    /**
     * @returns {PVec4} position of the sphere in 3d
     */
    getCenter() {
        return this.center;
    }
    /**
     * @param {PVec4} center 
     */
    setCenter(center) {
        this.center = center;
    }
    getRadius() {
        return this.radius;
    }
    setRadius(radius) {
        this.radius = radius;
    }

    static Test() {

        const sphere1 = new BoundingSphere(new PVec4(0, 0, 0), 1);
        const sphere2 = new BoundingSphere(new PVec4(0, 3, 0), 1);
        const sphere3 = new BoundingSphere(new PVec4(0, 0, 2), 1);
        const sphere4 = new BoundingSphere(new PVec4(1, 0, 0), 1);

        const sphere1IntersectSphere2 = sphere1.intersectBoundingShpere(sphere2);
        const sphere1IntersectSphere3 = sphere1.intersectBoundingShpere(sphere3);
        const sphere1IntersectSphere4 = sphere1.intersectBoundingShpere(sphere4);

        console.log("sphere1 intersects spehere2:", sphere1IntersectSphere2.getDoesIntersect())
        console.log("distance:", sphere1IntersectSphere2.getDistance());

        console.log("sphere1 intersects spehere3:", sphere1IntersectSphere3.getDoesIntersect())
        console.log("distance:", sphere1IntersectSphere3.getDistance());

        console.log("sphere1 intersects spehere4:", sphere1IntersectSphere4.getDoesIntersect())
        console.log("distance:", sphere1IntersectSphere4.getDistance());
    }
}