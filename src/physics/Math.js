class PMat4 {

    constructor() {

        this.m = [[], [], [], []];
        this.identity();
    }

    identity() {

        this.m[0][0] = 1; this.m[0][1] = 0; this.m[0][2] = 0; this.m[0][3] = 0;
        this.m[1][0] = 0; this.m[1][1] = 1; this.m[1][2] = 0; this.m[1][3] = 0;
        this.m[2][0] = 0; this.m[2][1] = 0; this.m[2][2] = 1; this.m[2][3] = 0;
        this.m[3][0] = 0; this.m[3][1] = 0; this.m[3][2] = 0; this.m[3][3] = 1;

        return this;
    }

    screenSpaceTransform(halfW, halfH) {

        this.m[0][0] = halfW; this.m[0][1] = 0; this.m[0][2] = 0; this.m[0][3] = halfW - 0.5;
        this.m[1][0] = 0; this.m[1][1] = -halfH; this.m[1][2] = 0; this.m[1][3] = halfH - 0.5;
        this.m[2][0] = 0; this.m[2][1] = 0; this.m[2][2] = 1; this.m[2][3] = 0;
        this.m[3][0] = 0; this.m[3][1] = 0; this.m[3][2] = 0; this.m[3][3] = 1;

        return this;
    }

    perspective(fov, aspectRatio, zNear, zFar) {

        const tanHalfFOV = Math.tan(fov / 2);
        const zRange = zNear - zFar;

        this.m[0][0] = 1 / (tanHalfFOV * aspectRatio); this.m[0][1] = 0; this.m[0][2] = 0; this.m[0][3] = 0;
        this.m[1][0] = 0; this.m[1][1] = 1 / tanHalfFOV; this.m[1][2] = 0; this.m[1][3] = 0;
        this.m[2][0] = 0; this.m[2][1] = 0; this.m[2][2] = (-zNear - zFar) / zRange; this.m[2][3] = 2 * zFar * zNear / zRange;
        this.m[3][0] = 0; this.m[3][1] = 0; this.m[3][2] = 1; this.m[3][3] = 0;

        return this;
    }

    multiply(r) {

        const res = new PMat4();

        for (let i = 0; i < 4; i++) {
            for (let j = 0; j < 4; j++) {
                res.set(i, j, this.m[i][0] * r.get(0, j) +
                    this.m[i][1] * r.get(1, j) +
                    this.m[i][2] * r.get(2, j) +
                    this.m[i][3] * r.get(3, j))
            }
        }

        return res;
    }

    translation(x, y, z) {
        this.m[0][0] = 1; this.m[0][1] = 0; this.m[0][2] = 0; this.m[0][3] = x;
        this.m[1][0] = 0; this.m[1][1] = 1; this.m[1][2] = 0; this.m[1][3] = y;
        this.m[2][0] = 0; this.m[2][1] = 0; this.m[2][2] = 1; this.m[2][3] = z;
        this.m[3][0] = 0; this.m[3][1] = 0; this.m[3][2] = 0; this.m[3][3] = 1;

        return this;
    }

    rotation(x, y, z) {

        const rx = new PMat4();
        const ry = new PMat4();
        const rz = new PMat4();

        rz.m[0][0] = Math.cos(z); rz.m[0][1] = -Math.sin(z); rz.m[0][2] = 0; rz.m[0][3] = 0;
        rz.m[1][0] = Math.sin(z); rz.m[1][1] = Math.cos(z); rz.m[1][2] = 0; rz.m[1][3] = 0;
        rz.m[2][0] = 0; rz.m[2][1] = 0; rz.m[2][2] = 1; rz.m[2][3] = 0;
        rz.m[3][0] = 0; rz.m[3][1] = 0; rz.m[3][2] = 0; rz.m[3][3] = 1;

        rx.m[0][0] = 1; rx.m[0][1] = 0; rx.m[0][2] = 0; rx.m[0][3] = 0;
        rx.m[1][0] = 0; rx.m[1][1] = Math.cos(x); rx.m[1][2] = -Math.sin(x); rx.m[1][3] = 0;
        rx.m[2][0] = 0; rx.m[2][1] = Math.sin(x); rx.m[2][2] = Math.cos(x); rx.m[2][3] = 0;
        rx.m[3][0] = 0; rx.m[3][1] = 0; rx.m[3][2] = 0; rx.m[3][3] = 1;

        ry.m[0][0] = Math.cos(y); ry.m[0][1] = 0; ry.m[0][2] = -Math.sin(y); ry.m[0][3] = 0;
        ry.m[1][0] = 0; ry.m[1][1] = 1; ry.m[1][2] = 0; ry.m[1][3] = 0;
        ry.m[2][0] = Math.sin(y); ry.m[2][1] = 0; ry.m[2][2] = Math.cos(y); ry.m[2][3] = 0;
        ry.m[3][0] = 0; ry.m[3][1] = 0; ry.m[3][2] = 0; ry.m[3][3] = 1;

        this.m = rz.multiply(ry.multiply(rx)).getM();

        return this;
    }

    //PVec4, PVec4
    rotationFU(forward, up) {

        const f = forward.normalized();
        const r = up.normalized();

        const tr = PVec4.crossVecs(r, f);
        const u = PVec4.crossVecs(f, tr);

        return this.rotationFUR(f, u, tr);
    }
    //PVec4, PVec4, Vec
    /**
     * 
     * @param {PVec4} forward 
     * @param {PVec4} up 
     * @param {PVec4} right 
     * @returns {PMat4} this
     */
    rotationFUR(forward, up, right) {
        const f = forward;
        const r = right;
        const u = up;

        this.m[0][0] = r.x; this.m[0][1] = r.y; this.m[0][2] = r.z; this.m[0][3] = 0;
        this.m[1][0] = u.x; this.m[1][1] = u.y; this.m[1][2] = u.z; this.m[1][3] = 0;
        this.m[2][0] = f.x; this.m[2][1] = f.y; this.m[2][2] = f.z; this.m[2][3] = 0;
        this.m[3][0] = 0; this.m[3][1] = 0; this.m[3][2] = 0; this.m[3][3] = 1;

        return this;
    }

    //float, float, float
    scale(x, y, z) {

        this.m[0][0] = x; this.m[0][1] = 0; this.m[0][2] = 0; this.m[0][3] = 0;
        this.m[1][0] = 0; this.m[1][1] = y; this.m[1][2] = 0; this.m[1][3] = 0;
        this.m[2][0] = 0; this.m[2][1] = 0; this.m[2][2] = z; this.m[2][3] = 0;
        this.m[3][0] = 0; this.m[3][1] = 0; this.m[3][2] = 0; this.m[3][3] = 1;

        return this;
    }

    //params: vec4, returns: PVec4
    transform(r) {

        return new PVec4(this.m[0][0] * r.getX() + this.m[0][1] * r.getY() + this.m[0][2] * r.getZ() + this.m[0][3] * r.getW(),
            this.m[1][0] * r.getX() + this.m[1][1] * r.getY() + this.m[1][2] * r.getZ() + this.m[1][3] * r.getW(),
            this.m[2][0] * r.getX() + this.m[2][1] * r.getY() + this.m[2][2] * r.getZ() + this.m[2][3] * r.getW(),
            this.m[3][0] * r.getX() + this.m[3][1] * r.getY() + this.m[3][2] * r.getZ() + this.m[3][3] * r.getW());
    }



    //GETTERS SETTERS
    get(x, y) { return this.m[x][y]; }
    getM() {
        const res = [[], [], [], []];
        for (let i = 0; i < 4; i++)
            for (let j = 0; j < 4; j++)
                res[i][j] = this.m[i][j];
        return res;
    }
    set(x, y, value) { this.m[x][y] = value; }
    setM(m) { this.m = m; }


}

class PVec4 {


    constructor(x = 0, y = 0, z = 0, w = -1) {

        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
    }

    /**
     * @param {number} r 
     * @returns {PVec4} result;
     */
    addN(r) {
        return new PVec4(this.x + r, this.y + r, this.z + r);
    }

    addV(other) {
        return new PVec4(this.x + other.x, this.y + other.y, this.z + other.z);
    }

    /**
     * @param {PVec4} other 
     */
    subtractV(other) {
        return new PVec4(this.x - other.x, this.y - other.y, this.z - other.z);
    }
    /**
     * @param {number} value 
     */
    subtractN(value) {
        return new PVec4(this.x - value, this.y - value, this.z - value);
    }

    multiplyN(r) {
        return new PVec4(this.x * r, this.y * r, this.z * r);
    }

    /**
     * @param {PVec4} other 
     */
    multiplyV(other) {
        return new PVec4(this.x * other.x, this.y * other.y, this.z * other.z);
    }

    /**
     * @param {PVec4} other 
     */
    plusEqualsV(other) {

        return new PVec4(
            this.x += other.x,
            this.y += other.y,
            this.z += other.z
        )
    }

    /**
     * @param {PVec4} other 
     */
    subEqualsV(other) {

        return new PVec4(
            this.x -= other.x,
            this.y -= other.y,
            this.z -= other.z
        );
    }

    /**
     * @param {number} value 
     */
    divideEqualsN(value) {
        return new PVec4(
            this.x /= value,
            this.y /= value,
            this.z /= value
        );
    }

    /**
     * @param {PVec4} other 
     */
    divideEqualsV(other) {
        return new PVec4(
            this.x /= other.x,
            this.y /= other.y,
            this.z /= other.z
        );
    }

    /**
     * @param {PVec4} normal 
     */
    reflect(normal) {

        const r = normal.multiplyN(this.dot(normal) * 2);
        return this.subtractV(r);
    };

    normalized() {
        const length = this.length();
        return new PVec4(this.x / length, this.y / length, this.z / length);
    }

    length() {
        return Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
    }

    maxV(other) {
        const res = new PVec4(
            Math.max(this.x, other.x),
            Math.max(this.y, other.y),
            Math.max(this.z, other.z)
        );
        return res;
    }

    /**
     * @param {PVec4} vec
     */
    cross(vec) {

        const x_ = this.y * vec.z - this.z * vec.y;
        const y_ = this.z * vec.x - this.x * vec.z;
        const z_ = this.x * vec.y - this.y * vec.x;

        return new PVec4(x_, y_, z_);
    }

    absolute() {

        return new PVec4(
            Math.abs(this.x),
            Math.abs(this.y),
            Math.abs(this.z)
        );
    }

    negate() {
        return new PVec4(-this.x, -this.y, -this.z);
    }

    /**
     * 
     * @param {*} axis 
     * @param {*} angle 
     */
    rotate(axis, angle) {

        angle = (angle / 180) * Math.PI;
        const sinAngle = Math.sin(-angle);
        const cosAngle = Math.cos(-angle);

        return this.cross(axis.multiplyN(sinAngle)).addV(              //Rotation on local X
            (this.multiplyN(cosAngle)).addV(                       //Rotation on local Z
                axis.multiplyN(this.dot(axis.multiplyN(1 - cosAngle)))));;
    }

    /**
     * 
     * @param {PQuat} rotation 
     */
    rotateQ(rotation) {

        const conjugate = rotation.conjugate();

		const w = rotation.multiplyVec(this).multiplyQuat(conjugate);

		return new PVec4(w.getX(), w.getY(), w.getZ(), 1.0);
    }

    /**
     * @param {PVec4} r 
     * @returns {number} 
     */
    dot(r) {
        return this.x * r.x + this.y * r.y + this.z * r.z;
    }

    max() {
        return Math.max(Math.max(this.x, this.y), this.z);
    }
}

class PQuat {

    constructor(data) {

		if(!data) {

			this.x = 0;
			this.y = 0;
			this.z = 0;
			this.w = 1;

			return;
		}

        switch(data.type) {

            case PQuat.Type.INIT_VALS:
                this.constructorWithVals(
                    data.x,
                    data.y,
                    data.z,
                    data.w
                )
                break;
            case PQuat.Type.INIT_VEC_ANGLE:

                this.constructorWithVecAngle(
                    data.vec,
                    data.angle
                )
		
                break;
            case PQuat.Type.INIT_MAT:

                this.constructorWithMat( data.mat )
                break;
			default:
				this.x = 0;
				this.y = 0;
				this.z = 0;
				this.w = 1;
        }
    }

    constructorWithVals(x, y, z, w) {

        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
    }

    constructorWithVecAngle(axis, angle) {

        var sinHalfAngle = parseFloat(Math.sin(angle / 2));
		var cosHalfAngle = parseFloat(Math.cos(angle / 2));

		this.x = axis.x * sinHalfAngle;
		this.y = axis.y * sinHalfAngle;
		this.z = axis.z * sinHalfAngle;
		this.w = cosHalfAngle;
    }

    constructorWithMat(rot) {

		var trace = rot.get(0, 0) + rot.get(1, 1) + rot.get(2, 2);

		if(trace > 0) {
			var s = 0.5 / parseFloat(Math.sqrt(trace + 1.0));
			this.w = 0.25 / s;
			this.x = (rot.get(1, 2) - rot.get(2, 1)) * s;
			this.y = (rot.get(2, 0) - rot.get(0, 2)) * s;
			this.z = (rot.get(0, 1) - rot.get(1, 0)) * s;
		}
		else {

			if(rot.get(0, 0) > rot.get(1, 1) && rot.get(0, 0) > rot.get(2, 2)) {

				var s = 2.0 * parseFloat(Math.sqrt(1.0 + rot.get(0, 0) - rot.get(1, 1) - rot.get(2, 2)));
				this.w = (rot.get(1, 2) - rot.get(2, 1)) / s;
				this.x = 0.25 * s;
				this.y = (rot.get(1, 0) + rot.get(0, 1)) / s;
				this.z = (rot.get(2, 0) + rot.get(0, 2)) / s;

			} else if(rot.get(1, 1) > rot.get(2, 2)) {

				var s = 2.0 * parseFloat(Math.sqrt(1.0 + rot.get(1, 1) - rot.get(0, 0) - rot.get(2, 2)));
				this.w = (rot.get(2, 0) - rot.get(0, 2)) / s;
				this.x = (rot.get(1, 0) + rot.get(0, 1)) / s;
				this.y = 0.25 * s;
				this.z = (rot.get(2, 1) + rot.get(1, 2)) / s;

			} else {

				var s = 2.0 * parseFloat(Math.sqrt(1.0 + rot.get(2, 2) - rot.get(0, 0) - rot.get(1, 1)));
				this.w = (rot.get(0, 1) - rot.get(1, 0) ) / s;
				this.x = (rot.get(2, 0) + rot.get(0, 2) ) / s;
				this.y = (rot.get(1, 2) + rot.get(2, 1) ) / s;
				this.z = 0.25 * s;
			}
		}

		var length = parseFloat(Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z + this.w * this.w));
		this.x /= length;
		this.y /= length;
		this.z /= length;
		this.w /= length;
	}

    length() {

		return parseFloat(Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z + this.w * this.w));
    }

    normalized() {

        var length = this.length();

        this.x /= length;
        this.y /= length;
        this.z /= length;
        this.w /= length;

        return this;
    }

    conjugate() {

        return new PQuat({
			type:PQuat.Type.INIT_VALS,
			x: -this.x,
			y: -this.y,
			z: -this.z,
			w: this.w
		});
    }

    //float 
	multiplyF(r) {
		return new PQuat({
			type: PQuat.Type.INIT_VALS,
			x: this.x * r,
			y: this.y * r,
			z: this.z * r,
			w: this.w * r
		});
	}

    
    multiplyQuat(r) {


		var w_ = this.w * r.getW() - this.x * r.getX() - this.y * r.getY() - this.z * r.getZ();
		var x_ = this.x * r.getW() + this.w * r.getX() + this.y * r.getZ() - this.z * r.getY();
		var y_ = this.y * r.getW() + this.w * r.getY() + this.z * r.getX() - this.x * r.getZ();
		var z_ = this.z * r.getW() + this.w * r.getZ() + this.x * r.getY() - this.y * r.getX();

        return new PQuat({
			type: PQuat.Type.INIT_VALS,
			x: x_,
			y: y_,
			z: z_,
			w: w_
		});
    }

    multiplyVec(vec) {
        
        var w_ = -this.x * vec.x - this.y * vec.y - this.z * vec.z;
        var x_ =  this.w * vec.x + this.y * vec.z - this.z * vec.y;
        var y_ =  this.w * vec.y + this.z * vec.x - this.x * vec.z;
        var z_ =  this.w * vec.z + this.x * vec.y - this.y * vec.x;

        return new PQuat({
			type: PQuat.Type.INIT_VALS,
			x: x_,
			y: y_,
			z: z_,
			w: w_
		});
    }

	subQuat(r) {
		return new PQuat({
			type: PQuat.Type.INIT_VALS,
			x: this.x - r.getX(),
			y: this.y - r.getY(),
			z: this.z - r.getZ(),
			w: this.w - r.getW()});
	}

	addQuat(r) {
		return new PQuat({
			type: PQuat.Type.INIT_VALS,
			x: this.x + r.getX(),
			y: this.y + r.getY(),
			z: this.z + r.getZ(),
			w: this.w + r.getW()});
	}

	toRotationMatrix() {

		var forward =  new PVec4(2.0 * (this.x * this.z - this.w * this.y), 2.0 * (this.y * this.z + this.w * this.x), 1.0 - 2.0 * (this.x * this.x + this.y * this.y));
		
		var up = new PVec4(2.0 * (this.x * this.y + this.w * this.z), 1.0 - 2.0 * (this.x * this.x + this.z * this.z), 2.0 * (this.y * this.z - this.w * this.x));
		var right = new PVec4(1.0 - 2.0 * (this.y * this.y + this.z * this.z), 2.0 * (this.x * this.y - this.w * this.z), 2.0 * (this.x * this.z + this.w * this.y));


		return new PMat4().rotationFUR(forward, up, right);
	}

	dot(r) {
		return this.x * r.getX() + this.y * r.getY() + this.z * r.getZ() + this.w * r.getW();
	}

    //PQuat, float, bool
	NLerp(dest, lerpFactor, shortest) {

		var correctedDest = dest;

		if(shortest && this.dot(dest) < 0)
			correctedDest = new PQuat({
				type: PQuat.Type.INIT_VALS,
				x: -dest.getX(),
				y: -dest.getY(),
				z: -dest.getZ(), 
				w: -dest.getW()
			});

		return correctedDest.subQuat(this).multiplyF(lerpFactor).addQuat(this).normalized();
	}

    //quat, quaternooin, float
	// static lerp(from, to, lerpFactor, shortest) {
		// var correctedDest = to;

		// if(shortest && from.dot(to) < 0) {
		// 	correctedDest = new PQuat({
		// 		type: PQuat.Type.INIT_VALS,
		// 		x: -dest.getX(),
		// 		y: -dest.getY(),
		// 		z: -dest.getZ(), 
		// 		w: -dest.getW()
		// 	});
		// }

		// return correctedDest.subQuat(from).multiplyF(lerpFactor).addQuat(from);
	// }

	static lerp(a,b,t){
		var ax = a.x,
			ay = a.y,
			az = a.z,
			aw = a.w;

		var out = new PQuat();
		out.x = ax + t * (b.x - ax);
		out.y = ay + t * (b.y - ay);
		out.z = az + t * (b.z - az);
		out.w = aw + t * (b.w - aw);
		return out;
	}

	static sLerp(a,b,t,shortest=false) {
		var EPSILON = 1e3;

		var cos = a.dot(b);
		var correctedDest = b;

		if(shortest && cos < 0) {
			cos = -cos;
			correctedDest = new PQuat({
				type: PQuat.Type.INIT_VALS,
				x: -b.getX(),
				y: -b.getY(),
				z: -b.getZ(),
				w: -b.getW()
			});
		}

		if(Math.abs(cos) >= 1 - EPSILON)
			return PQuat.nLerp(a, correctedDest, t, false);

		var sin = parseFloat(Math.sqrt(1.0 - cos * cos));
		var angle = parseFloat(Math.atan2(sin, cos));
		var invSin =  1.0 / sin;

		var srcFactor = parseFloat(Math.sin((1.0 - t) * angle)) * invSin;
		var destFactor = parseFloat(Math.sin((t) * angle)) * invSin;

		return a.multiplyF(srcFactor).addQuat(correctedDest.multiplyF(destFactor));
	}

	static nLerp(a,b,t, shortest) {

		var correctedDest = b;

		if(shortest && a.dot(b) < 0)
			correctedDest = new PQuat({
				type: PQuat.Type.INIT_VALS,
				x: -b.getX(),
				y: -b.getY(),
				z: -b.getZ(), 
				w: -b.getW()
			});

		return correctedDest.subQuat(a).multiplyF(t).addQuat(a).normalized();
	}

    //PQuat, float, boolean
	SLerp( dest, lerpFactor, shortest=false) {

		var EPSILON = 1e3;

		var cos = this.dot(dest);
		var correctedDest = dest;

		if(shortest && cos < 0) {
			cos = -cos;
			correctedDest = new PQuat({
				type: PQuat.Type.INIT_VALS,
				x: -dest.getX(),
				y: -dest.getY(),
				z: -dest.getZ(),
				w: -dest.getW()
			});
		}

		if(Math.abs(cos) >= 1 - EPSILON)
			return this.NLerp(correctedDest, lerpFactor, false);

		var sin = parseFloat(Math.sqrt(1.0 - cos * cos));
		var angle = parseFloat(Math.atan2(sin, cos));
		var invSin =  1.0 / sin;

		var srcFactor = parseFloat(Math.sin((1.0 - lerpFactor) * angle)) * invSin;
		var destFactor = parseFloat(Math.sin((lerpFactor) * angle)) * invSin;

		return this.multiplyF(srcFactor).addQuat(correctedDest.multiplyF(destFactor));
	}

    getForward() {

		return new PVec4(0,0,1,1).rotateQuat(this);
	}

	getBack() {
		return new PVec4(0,0,-1,1).rotateQuat(this);
	}

	getUp() {
		return new PVec4(0,1,0,1).rotateQuat(this);
	}

	getDown() {
		return new PVec4(0,-1,0,1).rotateQuat(this);
	}

	getRight() {
		return new PVec4(1,0,0,1).rotateQuat(this);
	}

	getLeft() {
		return new PVec4(-1,0,0,1).rotateQuat(this);
	}
	
	getX() {
		return this.x;
	}

	getY() {
		return this.y;
	}

	getZ() {
		return this.z;
	}

	getW() {

		return this.w;
	}

    //PQuat
	equals(r){
		return this.x === r.getX() && this.y === r.getY() && this.z === r.getZ() && this.w === r.getW();
	}

	//params: PVec4, PVec4 / returns: PVec4
	static lookRotation(vDir, vUp){
		var zAxis	= vDir,	//Forward
			up		= vUp,
			xAxis	= new PVec4(),		//Right
			yAxis	= new PVec4();
		
		zAxis = zAxis.normalized();
		xAxis = zAxis.cross(up);
		
		xAxis = xAxis.normalized();
		yAxis = zAxis.cross(xAxis); //new up

		//fromAxis - Mat3 to PQuat
		var m00 = xAxis.x, m01 = xAxis.y, m02 = xAxis.z,
			m10 = yAxis.x, m11 = yAxis.y, m12 = yAxis.z,
			m20 = zAxis.x, m21 = zAxis.y, m22 = zAxis.z,
			t = m00 + m11 + m22,
			x, y, z, w, s;



		if(t > 0.0){
			s = Math.sqrt(t + 1.0);
			w = s * 0.5 ; // |w| >= 0.5
			s = 0.5 / s;
			x = (m12 - m21) * s;
			y = (m20 - m02) * s;
			z = (m01 - m10) * s;
		} else if((m00 >= m11) && (m00 >= m22)){
			s = Math.sqrt(1.0 + m00 - m11 - m22);
			x = 0.5 * s;// |x| >= 0.5
			s = 0.5 / s;
			y = (m01 + m10) * s;
			z = (m02 + m20) * s;
			w = (m12 - m21) * s;
		} else if(m11 > m22){
			s = Math.sqrt(1.0 + m11 - m00 - m22);
			y = 0.5 * s; // |y| >= 0.5
			s = 0.5 / s;
			x = (m10 + m01) * s;
			z = (m21 + m12) * s;
			w = (m20 - m02) * s;
		} else{
			s = Math.sqrt(1.0 + m22 - m00 - m11);
			z = 0.5 * s; // |z| >= 0.5
			s = 0.5 / s;
			x = (m20 + m02) * s;
			y = (m21 + m12) * s;
			w = (m01 - m10) * s;
		}

		var out = new PVec4();

		out.x = x;
		out.y = y;
		out.z = z;
		out.w = w;

		return out;
	}
}

PQuat.Type = {
    INIT_VALS: 0,
    INIT_VEC_ANGLE: 1,
    INIT_MAT: 2
}