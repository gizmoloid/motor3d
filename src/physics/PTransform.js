class PTransform {

    //PVec4, Qauternion, PVec4
    constructor(pos = new PVec4(), rot = new PQuat(), scale = new PVec4(1, 1, 1)) {

        this.pos = pos;
        this.rot = rot;
        this.scale = scale;
    }

    clone() {

        return new PTransform(this.pos, this.rot, this.scale);
    }

    //params: PVec4 returns: PTransform
    setPos(pos) {
        return new PTransform(pos, this.rot, this.scale);
    }

    /**
     * @param {PQuat} rotation 
     * @returns {PTransform} new PTransform
     */
    rotate(rotation) {
        return new PTransform(this.pos, rotation.multiplyQuat(this.rot).normalized(), this.scale);
    }

    //params: PVec4 / returns: PTransform
    setScale(scl) {

        return new PTransform(this.pos, this.rot, scl);
    }

    //params: PVec4, PVec4 / returns: PTransform
    lookAt(point, up) {
        return this.rotate(this.getLookAtRotation(point, up));
    }

	/**
	 * @param {PVec4} point
	 * @param {PVec4} up
	 */
    getLookAtRotation(point, up) {

        const mat = new PMat4();
        const v = point.subtractV(this.pos).normalized();

        return new PQuat({
            type: PQuat.Type.INIT_MAT,
            mat: mat.rotationFU(v, up)
        });
    }

    //PMat4
    getTransformation() {


        const translationMatrix = new PMat4().translation(this.pos.x, this.pos.y, this.pos.z);
        const rotationMatrix = this.rot.toRotationMatrix();

        const scaleMatrix = new PMat4().scale(this.scale.x, this.scale.y, this.scale.z);

        return translationMatrix.multiply(rotationMatrix.multiply(scaleMatrix));
        // return rotationMatrix.multiply(translationMatrix.multiply(scaleMatrix));
    }

    getTransformedPos() {
        return this.pos;
    }

    getTransformedRot() {
        return this.rot;
    }

    getPos() {
        return this.pos;
    }

    getRot() {
        return this.rot;
    }

    getScale() {
        return this.scale;
    }
}