class PhysicsObject {

    /**
     * @param {number} id 
     * @param {PVec4} position 
     * @param {PVec4} velocity 
     * @param {number} radius 
     */
    constructor(id = -1, collider = new Collider(), velocity = new PVec4()) {

        this.id = id;

        this.collider = collider;

        this.transform = new PTransform();

        this.position = this.collider.getCenter();
        this.oldPosition = this.position;
        this.velocity = velocity;

        this.rotation = 0;

        this.maxVelocity = 0.4;
        this.maxRot = 15;

        this.drag = 0.001;

        this.isBreaking = false;
    }

    /**
     * @param {float} delta 
     */
    integrate(delta) {

        this.velocity.z -= this.drag;
        if(this.velocity.z < 0) this.velocity.z = 0;

        // this.velocity.x = this.velocity.z;

        // this.rotation -= this.drag;

        const r = new PQuat({
            type: PQuat.Type.INIT_VEC_ANGLE,
            vec: new PVec4(0,1,0),
            angle: (this.rotation / 180) * Math.PI
        });

        this.position = this.position.plusEqualsV(this.velocity.multiplyN(delta));

        // this.position.rotateN(this.rotation)


        // this.position = this.position.addV(this.velocity.multiplyN(delta));
        // this.position.plusEqualsV(this.velocity.multiplyN(delta));

        this.transform = this.transform.rotate(r);
        this.transform = this.transform.setPos(this.position);

        // this.position = this.position.rotateQ(r);
    }

    resetRot() {
        this.rotation = 0;
    }

    accelerate(tick) {

        this.velocity.z += 0.01;

        if(this.velocity.z > this.maxVelocity) this.velocity.z = this.maxVelocity;
    }

    doBreak() {

        this.velocity.z -= 0.02;
    }

    turnLeft() {

        this.rotation -= (0.1 * (this.velocity.z / this.maxVelocity));
        if(this.rotation < -this.maxRot) this.rotation = -this.maxRot;
    }
    turnRight() {

        this.rotation += (0.1 * (this.velocity.z / this.maxVelocity));
        if(this.rotation > this.maxRot) this.rotation = this.maxRot;
    }

    getCollider() {

        const translation = this.position.subtractV(this.oldPosition);
        this.oldPosition = this.position;
        this.collider.transform(translation);

        return this.collider;
    }

    getPosition() {
        return this.position;
    }
    setPosition(position) {
        this.position = position;
    }
    getVelocity() {
        return this.velocity;
    }
    setVelocity(velocity) {
        this.velocity = velocity;
    }
}