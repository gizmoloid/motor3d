class Collider {

    /**
     * @param {Collider.Type} type 
     */
    constructor(type) {
        this.type = type;
    }

    /**
     * 
     * @param {Collider} other 
     */
    intersect(other) {

        if(this.getType() === Collider.Type.SPHERE && other.getType() === Collider.Type.SPHERE) {
            return this.intersectBoundingSphere(other);
        }

        if(this.getType() === Collider.Type.AABB && other.getType() === Collider.Type.AABB) {
            return this.testAABB(other);
        }
        throw new Error("Collisions not implemented between specified colliders!");
    }
    /**
     * 
     * @param {PVec4} translation 
     * 
     * @returns {Collider}
     */
    transform(translation, rotation, scale) {
        return new Collider();
    }

    getCenter() {
        return new PVec4();
    }

    getType() {
        return this.type;
    }
    /**
     * @param {Collider} type 
     */
    setType(type) {
        this.type = type;
    }
}

Collider.Type = {

    SPHERE: "collidersphere",
    AABB: "collideraxisalignedboundingbox",
    SIZE: 2
}