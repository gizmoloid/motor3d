class PhysicsEngine {

    constructor() {

        this.physicObjects = [];
        this.isRunning = false;

        this.intervalID = null;
    }

    update(data) {

        switch (data.action) {
            case PhysicsEngine.Actions.RUN:
                this.run();
                break;
            case PhysicsEngine.Actions.PAUSE:
                this.pause();
                break;
            case PhysicsEngine.Actions.ADD_OBJECTS:
                this.addObjects(data.objects);
                break;
        }
    }

    /**
     * @param {Array} objects 
     */
    addObjects(objects) {

        for (let i = 0; i < objects.length; i++) {
            const obj = objects[i];

            const vel = 0.01;

            const velocity = new PVec4(0, 0, 0);

            const pos = obj.transform.pos;
            const collider = new AABB(new PVec4(pos.x, pos.y, pos.z));

            this.physicObjects.push(new PhysicsObject(obj.id, collider, velocity));
        }
    }

    run() {
        this.isRunning = true;
        const SPEED = 0.05;
        this.intervalID = setInterval(() => {
            this.simulate(0.05);
        }, 1);
    }

    pause() {
        this.isRunning = false;
        clearInterval(this.intervalID);
        this.intervalID = null;
    }

    simulate(delta) {

        this.physicObjects.forEach(pObj => {
            pObj.integrate(delta);
        });
        this.handleCollisions();
    }

    handleCollisions() {

        const len = this.physicObjects.length;

        for (let i = 0; i < len; i++) {
            const iObjCol = this.physicObjects[i];

            for (let j = i + 1; j < len; j++) {
                const jObjCol = this.physicObjects[j];
                const collisionData = iObjCol.getCollider().intersect(jObjCol.getCollider());

                if (collisionData.getDoesCollide()) {
                    const direction = collisionData.getDirection().normalized();
                    const reversedDirection = direction.reflect(iObjCol.getVelocity().normalized());
                    iObjCol.setVelocity(iObjCol.getVelocity().reflect(reversedDirection));
                    jObjCol.setVelocity(jObjCol.getVelocity().reflect(direction));
                }
            }
        }
    }

    getObjects(input) {

        this.physicObjects.forEach(obj => {
            if (input.up) {
                obj.accelerate(0.05);
            } else if (input.down) {
                obj.doBreak();
            }

            if (input.left) {
                obj.turnLeft();
            } else if (input.right) {
                obj.turnRight();
            } else {
                obj.resetRot();
            }
        });

        return this.physicObjects[0].transform;
    }

    getObject(index) {
        return this.physicObjects[index];
    }
    getNumObjets() {
        return this.physicObjects.length;
    }
}

PhysicsEngine.Actions = {
    RUN: "run",
    PAUSE: "pause",
    ADD_OBJECTS: "addobjects",
    GET_SIMULATION: "getsimulation"
}