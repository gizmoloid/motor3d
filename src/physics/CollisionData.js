class CollisionData {
    constructor(doesCollide = null, direction = new PVec4()) {

        this.doesCollide = doesCollide;
        this.direction = direction;
    }

    getDoesCollide() {
        return this.doesCollide;
    }
    setDoesCollide(doesCollide) {
        this.doesCollide = doesCollide;
    }

    getDirection() {
        return this.direction;
    }
    setDirection(direction) {
        this.direction = direction;
    }
}