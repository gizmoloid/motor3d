class Plane {

    constructor(normal = null, distance = null) {

        this.normal = normal;
        this.distance = distance;
    }

    normalized() {

        const magnitude = this.normal.length();

        return new Plane(this.normal / magnitude, this.distance / magnitude);
    }

    /**
     * @param {BoundingSphere} other 
     */
    intersectSphere(other) {
        const distanceFromSphereCenter = Math.abs(this.normal.dot(other.getCenter()) + this.distance);
        const distanceFromSphere = distanceFromSphereCenter - other.getRadius();
        return new IntersectData(distanceFromSphere < 0, this.normal.multiplyV(distanceFromSphere));
    }

    getNormal() {
        return this.normal;
    }
    setNormal(normal) {
        this.normal = normal;
    }

    getDistance() {
        return this.distance;
    }
    setDistance(distance) {
        this.distance = distance;
    }
}