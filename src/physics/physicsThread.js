
const PHYSICS_PATH = "/src/physics/";
self.importScripts( PHYSICS_PATH + "Math.js" );
self.importScripts( PHYSICS_PATH + "Collider.js" );
self.importScripts( PHYSICS_PATH + "CollisionData.js" );
self.importScripts( PHYSICS_PATH + "BoundingSphere.js" );
self.importScripts( PHYSICS_PATH + "PTransform.js" );
self.importScripts( PHYSICS_PATH + "AABB.js" );
self.importScripts( PHYSICS_PATH + "Plane.js" );
self.importScripts( PHYSICS_PATH + "PhysicsObject.js" );
self.importScripts( PHYSICS_PATH + "PhysicsEngine.js" );

const physics = new PhysicsEngine();
self.addEventListener("message", (event) => {

    if(event.data.action === PhysicsEngine.Actions.GET_SIMULATION) {
        self.postMessage({objects:physics.getObjects(event.data.data)});
    } else {
        physics.update(event.data);
    }
    
}, false);
