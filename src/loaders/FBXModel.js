import Model3D from "./Model3D.js";

class FBXModel extends Model3D {
    constructor(data) {
        super(data);
    }
}

export default Model3D;