import Mat4 from "../core/math/Mat4.js";
import Input from "../input/Input.js";
import Vec4 from "../core/math/Vec4.js";
import Transform from "./Transform.js";
import Util from "../utils/Util.js";
import Keyboard from "../input/Keyboard.js";
import Quaternion from "../core/math/Quaternion.js";

class Camera3D {

	constructor(fov, aspectRatio, zNear = 0.01, zFar = 1000.0) {

		this.projection = new Mat4().perspective(fov, aspectRatio, zNear, zFar);
		this.transform = new Transform();
	}
	
	resetTarget(target) {

		this.projection = new Mat4().perspective(parseFloat(Utils.toRadians(70.0)),
			parseFloat(target.getWidth()) / parseFloat(target.getHeight()),
			0.1,
			1000.0);
	}

	reset() {

		this.transform = new Transform();
	}

	//PARAMS: float
	setPerspective(angle) {

		this.projection = new Mat4().perspective(parseFloat(Utils.toRadians(angle)),
			parseFloat(target.getWidth()) / parseFloat(target.getHeight()),
			0.1,
			1000.0);
	}

	getViewProjection() {

		//Mat4
		const cameraRotation = this.getTransform().getTransformedRot().conjugate().toRotationMatrix();
		//Vec4
		const cameraPos = this.getTransform().getTransformedPos().multiply(-1);
		//Mat4
		const cameraTranslation = new Mat4().translation(cameraPos.getX(), cameraPos.getY(), cameraPos.getZ());

		return this.projection.multiply(cameraRotation.multiply(cameraTranslation));
	}

	//Input, float
	updateInput(delta) {

		// Speed and rotation amounts are hardcoded here.
		// In a more general system, you might want to have them as variables.
		//floats
		const sensitivityX = 1.5 * delta;
		const sensitivityY = 1 * delta;
		const movAmt = 5.0 * delta;

		if(Input.getKeyDown(Keyboard.Keys.W)) {
			this.move(this.transform.getRot().getForward(), movAmt);
		} else if(Input.getKeyDown(Keyboard.Keys.S)) {
			this.move(this.transform.getRot().getForward(), -movAmt);
		}

		if(Input.getKeyDown(Keyboard.Keys.A)) {
			this.move(this.transform.getRot().getLeft(), movAmt);
		} else if(Input.getKeyDown(Keyboard.Keys.D)) {
			this.move(this.transform.getRot().getRight(), movAmt);
		}

		if(Input.getKeyDown(Keyboard.Keys.UP)) {
			this.rotate(this.getTransform().getRot().getRight(), sensitivityY);
		} else if(Input.getKeyDown(Keyboard.Keys.DOWN)) {
			this.rotate(this.getTransform().getRot().getRight(), -sensitivityY);
		}

		if(Input.getKeyDown(Keyboard.Keys.LEFT)) {
			this.rotate(Camera3D.Y_AXIS, -sensitivityX);
		} else if(Input.getKeyDown(Keyboard.Keys.RIGHT)) {
			this.rotate(Camera3D.Y_AXIS, sensitivityX);
		}
	}

	moveTo(to, tick) {

		this.transform = this.getTransform().setPos(this.getTransform().getPos().lerp(to, tick));
		// if (this.getTransform().getPos() >= to.z) {
		// 	this.getTransform().getPos().z = to.z;
		// 	return;
		// }
	}

	/**
	 * 
	 * @param {Vec4} dir 
	 * @param {number} amount 
	 */
	move(dir, amt) {

		this.transform = this.transform.setPos(this.transform.getPos().addV(dir.multiply(amt)));
	}

	//Vec4, float
	rotate(axis, angle) {

		this.transform = this.getTransform().rotate(new Quaternion({
			type: Quaternion.Type.INIT_VEC_ANGLE,
			vec: axis,
			angle: angle
		}));
	}

	lookAt(point, up) {
		this.transform = this.transform.lookAt(point, up);
	}

	//returns Transform
	getTransform() {
		return this.transform;
	}
}

Camera3D.Y_AXIS = new Vec4(0, 1, 0);
Camera3D.FOV = parseFloat(Util.toRadians(70.0));

export default Camera3D;