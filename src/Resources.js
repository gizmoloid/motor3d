import OBJModel from "./loaders/ObjModel.js";
import Mesh from "./components/Mesh.js";
import Texture from "./core/Texture.js";
import Bitmap from "./core/Bitmap.js";

class Resources {

    constructor(data) { }

    static async loadData(dataURL) {
        return await fetch(Resources.PATH + dataURL).
            then(result => {
                return result.json();
            }).
            then(data => {
                Resources.Data = data;
                console.log("0")
                return Resources.loadMeshes(Resources.Data.meshes);
            }).
            then(meshesData => {
                console.log("1")
                Resources.Meshes = meshesData;
                return Resources.loadTextures(Resources.Data.textures);
            }).
            then(texturesData => {
                console.log("2")
                Resources.Textures = texturesData;
                return Resources.loadAudios(Resources.Data.audios);
            }).
            then(audiosData => {
                Resources.Audios = audiosData;
                return true;
            }).
            catch(error => {
                throw new Error(error);
            });
    }

    static async loadMeshes(meshesData) {

        const urls = [];
        for (let i = 0; i < meshesData.length; i++) {
            urls.push(Resources.MESHES_PATH + meshesData[i].src);
        }

        const meshes = [];
        return Promise.all(urls.map(url =>
            fetch(url).then(resp => resp.text())
        )).then(texts => {


            for (let i = 0; i < meshesData.length; i++) {
                const mesh = meshesData[i];
                let m = null;
                switch (mesh.type) {
                    case Resources.FileTypes.OBJ:

                        m = new Mesh(new OBJModel(texts[i]).toIndexedModel());

                        break;
                    case Resources.FileTypes.FBX:
                        break;
                }

                if (m) meshes.push(m);
            }
            return meshes;
        }).
            catch(error => {
                throw new Error(error);
            });
    }

    static async loadTextures(texturesData) {

        const textures = [];
        return new Promise((resolve, reject) => {

            const canvas = document.createElement("canvas");
            const ctx = canvas.getContext("2d");

            const imageLoaded = (img) => {

                canvas.width = img.width;
                canvas.height = img.height;
                ctx.drawImage(img, 0, 0);

                const srcData = ctx.getImageData(0, 0, img.width, img.height);
                const copiedData = new Uint8ClampedArray(srcData.data.length);
                copiedData.set(srcData.data);

                const bitmap = new Bitmap({
                    width: img.width,
                    height: img.height,
                    imgData: copiedData
                });

                textures.push(new Texture({
                    id: img.id,
                    bitmap: bitmap,
                    name: img.name
                }));
                if (textures.length === texturesData.length) {
                    resolve(textures);
                }
            }

            for (let i = 0; i < texturesData.length; i++) {
                const image = new Image();
                image.name = texturesData[i].name;
                image.id = texturesData[i].id;
                image.onload = () => {
                    imageLoaded(image);
                }
                image.src = Resources.TEXTURES_PATH + texturesData[i].src
            }
        });
    }

    static async loadAudios(audiosData) {
        return "audios";
    }


    //GETTERS
    static getMeshes(meshIDs) {
        const meshes = [];
        for (let i = 0; i < meshIDs.length; i++) {
            meshes.push(Resources.Meshes[meshIDs[i]]);
        }
        return meshes;
    }
    static getMesh(meshID) {
        return Resources.Meshes[meshID];
    }
    static getTextures(textureIDs) {
        const textures = [];
        for (let i = 0; i < textureIDs.length; i++) {
            textures.push(Resources.Textures[textureIDs[i]]);
        }
        return textures;
    }
    static getTexture(textureID) {
        if (typeof (textureID) === "number") {
            return Resources.Textures[textureID];
        } else if (typeof (textureID === "string")) {
            for (let i = 0; i < Resources.Textures.length; i++) {
                const texture = Resources.Textures[i];
                if (texture.name === textureID) return texture;
            }
        }
    }
}

Resources.Data = null;
Resources.Meshes = null;
Resources.Textures = null;
Resources.Audios = null;
Resources.PATH = "./resources/";
Resources.MESHES_PATH = "./resources/meshes/";
Resources.TEXTURES_PATH = "./resources/textures/";
Resources.AUDIOS_PATH = "./resources/audios/";

Resources.FileTypes = {
    FBX: "fbx",
    OBJ: "obj",
    JPG: "jpg",
    PNG: "png",
    MP3: "mp3",
    OGG: "ogg"
}

export default Resources;