import Resources from "../Resources.js";
import Color from "../core/Color.js";

class Game {

    constructor() {


        this.meshes = null;
        this.renderer = null;
        this.camera = null;
        this.gameObjects = [];
    }

    async setData(gameDataURL) {

        return await fetch(Resources.PATH + gameDataURL).
            then(result => {
                return result.json();
            }).
            then(jsonData => {
                this.meshes = Resources.getMeshes(jsonData.meshes);

                return this;
            }).
            catch(error => {
                throw new Error(error);
            });
    }

    setRenderer(renderer) {
        this.renderer = renderer;
    }

    updatePhysics(data) {

        
    }

    update(tick) {

        // this.camera.updateInput(tick);
        this.reset();
    }

    reset() {

    }

    getGameObjects() {
        return this.gameObjects;
    }

    render(tick) {

        this.renderer.clear(Color.red);
        this.renderer.clearDepthBuffer();

        // this.currentFX(tick);
        // for(var i = 0; i < this.meshes.length; i++) {
        //     this.meshes[i].update(this.renderer, viewPerspective, tick);
        // }
    }
}

export default Game;