import Game from "./Game.js";
import Camera3D from "../components/Camera3D.js";
import GameObject from "./GameObject.js";
import Resources from "../Resources.js";
import Vec4 from "../core/math/Vec4.js";
import Quaternion from "../core/math/Quaternion.js";
import Mat4 from "../core/math/Mat4.js";
import Transform from "../components/Transform.js";

class GameTemplate extends Game {

    constructor(data) {
        super(data);

        this.car = new GameObject({
            mesh: Resources.getMesh(1).clone(),
            texture: Resources.getTexture(1).clone(),
            rad: 1,
            id: 1
        });

        this.gameObjects.push(this.car);
    }

    /**
     * OVERRIDES
     */
    getGameObjects() {
        return this.gameObjects;
    }
    setRenderer(renderer) {
        super.setRenderer(renderer);
        this.camera = new Camera3D(Camera3D.FOV, this.renderer.getWidth() / this.renderer.getHeight());
        this.camera.move(new Vec4(0, 0, 1), -2);
        this.camera.move(new Vec4(0, 1, 0), 1.5);
        this.camera.lookAt(this.car.getPos(), Vec4.up());
    }
    updatePhysics(data) {
        super.updatePhysics(data);

        const p = data.objects.pos;
        const r = data.objects.rot;
        const s = data.objects.scale;


        this.car.setTransform(new Transform(
            new Vec4(p.x, p.y, p.z),
            new Quaternion({
                type: Quaternion.Type.INIT_VALS,
                x: r.x,
                y: r.y,
                z: r.z,
                w: r.w
            }),
            new Vec4(s.x, s.y, s.y)
        ));
        // this.car.setPos(new Vec4(p.x, p.y, p.z));
        // this.car.setRotate(new Quaternion({
        //     type: Quaternion.Type.INIT_VALS,
        //     x: r.x,
        //     y: r.y,
        //     z: r.z,
        //     w: r.w
        // }));
        // this.car.setScale(new Vec4(s.x, s.y, s.y));

        // this.car.setTransformMatrix(tm);
        // this.road.setTransformMatrix();
        

        // for (let i = 0; i < data.objects.length; i++) {
        //     const pos = data.objects[i].position;

        //     this.gameObjects[i].setPos(new Vec4( pos.x, pos.y, pos.z));
        // }
    }
    update(tick) {
        super.update(tick);

        const offset = new Vec4();
        offset.x = 0;
        offset.y = 1;
        offset.z = -1;

        this.camera.updateInput(tick);

        // this.camera.moveTo(this.car.getPos().addV(offset), tick);
    }

    render(tick) {

        super.render(tick);
        // this.road.update(this.renderer, this.camera.getViewProjection(), tick);
        this.car.update(this.renderer, this.camera.getViewProjection(), tick);
        this.car.reset();
    }
}

export default GameTemplate;